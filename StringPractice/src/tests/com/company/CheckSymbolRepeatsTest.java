package tests.com.company;

import main.com.company.CheckSymbolRepeats;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;

public class CheckSymbolRepeatsTest {
    private Scanner scanner = Mockito.mock(Scanner.class);
    CheckSymbolRepeats cut = new CheckSymbolRepeats(scanner);
    static Arguments[] checkSymbolRepeatsTestArgs(){
        return new Arguments[]{
                Arguments.arguments("aaakkfff", "k", 2),
                Arguments.arguments("aaafff", "k", 0),
                Arguments.arguments("akaakkffkf", "k", 4)
        };
    }
    @ParameterizedTest
    @MethodSource("checkSymbolRepeatsTestArgs")
    void checkSymbolRepeatsTest(String str1, String str2, int expCounter) {
        Mockito.when(scanner.nextLine()).thenReturn(str1, str2);
        String expected = "Symbol " + str2 + " occurs in string " + expCounter + " time.";
        String actual = cut.checkSymbolRepeats();
        Assertions.assertEquals(expected, actual);
    }
}
