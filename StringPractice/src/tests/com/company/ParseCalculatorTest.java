package tests.com.company;

import main.com.company.ParseCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ParseCalculatorTest {
    private final ParseCalculator cut = new ParseCalculator();
    static Arguments[] parseCalculatorTestArgs(){
        return new Arguments[]{
                Arguments.arguments("2+7", true),
                Arguments.arguments("2-7", true),
                Arguments.arguments("2/7", true),
                Arguments.arguments("2*7", true),
                Arguments.arguments("2++7", false),
                Arguments.arguments("asd?2+7", false),
        };
    }

    static Arguments[] calculateTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, 2, '+', "1 + 2 = 3"),
                Arguments.arguments(3, 2, '-',  "3 - 2 = 1"),
                Arguments.arguments(3, 2, '*', "3 * 2 = 6"),
                Arguments.arguments(4, 2, '/', "4 / 2 = 2")

        };
    }

    @ParameterizedTest
    @MethodSource("parseCalculatorTestArgs")
    void parseCalculatorTest(String str, boolean expected){
        boolean actual = cut.parseCalculator(str);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("calculateTestArgs")
    void calculateTest(int num1, int num2, char operation, String expected){
        String actual = cut.calculate(num1, num2, operation);
        Assertions.assertEquals(expected, actual);
    }
}
