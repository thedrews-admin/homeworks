package main.com.company;

import java.util.Scanner;

public class CheckSymbolRepeats {
        public Scanner scanner;

    public CheckSymbolRepeats(Scanner scanner) {
        this.scanner = scanner;
    }

    public String checkSymbolRepeats() {
            System.out.println("Enter string: ");
            String str = scanner.nextLine();
            System.out.println("Enter symbol: ");
            String chr = scanner.nextLine();
            int count = 0;
        String[] charArr = str.split("");

        for (int i = 0; i < charArr.length; i++) {
            if (charArr[i].equals(chr)) {
                count++;
            }
        }

        return new String("Symbol " + chr + " occurs in string " + count + " time.");
    }
}

