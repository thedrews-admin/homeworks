package main.com.company;

public class ParseCalculator {
    int indexOfOperation;
    boolean isDigit = true;
    String firstHalf, secondHalf;
    char[] firstHalfArray, secondHalfArray;
    int firstNum, secondNum;
    char operation;
    public boolean parseCalculator(String str){
        if(str.contains("+")){
            operation = '+';
            indexOfOperation = str.indexOf("+");
            firstHalf = str.substring(0, indexOfOperation);
            secondHalf = str.substring(indexOfOperation+1);

            firstHalfArray = firstHalf.toCharArray();
            secondHalfArray = secondHalf.toCharArray();

            for(char i : firstHalfArray){
                if(Character.isDigit(i)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }

            System.out.println();
            for(char a : secondHalfArray){
                if(Character.isDigit(a)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }
            if(isDigit != false){
                firstNum = Integer.parseInt(firstHalf);
                secondNum = Integer.parseInt(secondHalf);
            }

        }else if(str.contains("-")){
            operation = '-';
            indexOfOperation = str.indexOf("-");
            firstHalf = str.substring(0, indexOfOperation);
            secondHalf = str.substring(indexOfOperation+1);

            firstHalfArray = firstHalf.toCharArray();
            secondHalfArray = secondHalf.toCharArray();

            for(char i : firstHalfArray){
                if(Character.isDigit(i)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }

            System.out.println();
            for(char a : secondHalfArray){
                if(Character.isDigit(a)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }

            if(isDigit != false){
                firstNum = Integer.parseInt(firstHalf);
                secondNum = Integer.parseInt(secondHalf);
            }

        }else if(str.contains("/")){
            indexOfOperation = str.indexOf("/");
            operation = '/';
            firstHalf = str.substring(0, indexOfOperation);
            secondHalf = str.substring(indexOfOperation+1);

            firstHalfArray = firstHalf.toCharArray();
            secondHalfArray = secondHalf.toCharArray();

            for(char i : firstHalfArray){
                if(Character.isDigit(i)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }

            System.out.println();
            for(char a : secondHalfArray){
                if(Character.isDigit(a)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }
            if(isDigit != false){
                firstNum = Integer.parseInt(firstHalf);
                secondNum = Integer.parseInt(secondHalf);
            }
        }else if(str.contains("*")){
            operation = '*';
            indexOfOperation = str.indexOf("*");
            firstHalf = str.substring(0, indexOfOperation);
            secondHalf = str.substring(indexOfOperation+1);

            firstHalfArray = firstHalf.toCharArray();
            secondHalfArray = secondHalf.toCharArray();

            for(char i : firstHalfArray){
                if(Character.isDigit(i)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }

            System.out.println();
            for(char a : secondHalfArray){
                if(Character.isDigit(a)){
                    System.out.println("digit");
                }else{
                    System.out.println("not digit");
                    isDigit = false;
                }
            }
            if(isDigit != false){
                firstNum = Integer.parseInt(firstHalf);
                secondNum = Integer.parseInt(secondHalf);
            }
        }
        if(isDigit == false){
            return false;
        }else return true;
    }

    public String calculate(int firstNum, int secondNum, char operation){
        String sout = "" + firstNum + " " + operation + " " + secondNum + " = ";
                int result = 0;
        if(operation == '+') {
            result = firstNum + secondNum;
        }    
        if(operation == '-'){
            result = firstNum - secondNum;
        }
        if(operation == '/'){
            result = firstNum / secondNum;
        }
        if(operation == '*'){
            result = firstNum * secondNum;
        }
        
        return sout + result;
    }

}

