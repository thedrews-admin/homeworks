package tests.com.company;

import main.com.company.Difference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class DifferenceTest {
    Difference cut = new Difference();
    static Arguments[] getDifferenceTestArgs(){
        return new Arguments[]{
                Arguments.arguments(50, 30, 20),
                Arguments.arguments(1, 1, 0),
                Arguments.arguments(100, 100, 0),
                Arguments.arguments(100, 35, 65),
                Arguments.arguments(35, 100, 65),
        };
    }

    @ParameterizedTest
    @MethodSource("getDifferenceTestArgs")
    void getDifferenceTest(int numToGuess, int userGuess, int expDiff){
        int actual = cut.getDifference(numToGuess, userGuess);
        Assertions.assertEquals(actual, expDiff);
    }
}
