package main.com.company;

public class HotOrCold {
    public String isHotOrCold(int oldDifference, int currentDifference) {
        if (currentDifference > oldDifference) {
            return "Colder";
        }else if(currentDifference < oldDifference){
            return "Hotter";
        }else if(currentDifference == oldDifference){
            return "Nothing changed";
        }else return "else";
    }
}