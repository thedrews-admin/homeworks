package main.com.company;

import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        UserResponse userResponse = new UserResponse();
        Randomizer randomizer = new Randomizer();
        HotOrCold hotOrCold = new HotOrCold();
        Difference difference = new Difference();
        int userNum, minNum = 1, maxNum = 100;
        int counter = 0, finalCounter = 5, differenceNum, oldDifference = 0, currentDifference, tempDiff;
        int interval = maxNum - (minNum-1);
        int numToGuess = randomizer.randomize(minNum, interval);
        String hotCold;
        System.out.println(numToGuess);
        while(counter < finalCounter) {
            do {
                System.out.println("Enter num from 1 to 100:");
                String userInput = scanner.next();
                userNum = userResponse.getUserNum(userInput);
            } while (userNum == 0);
            if(userNum == numToGuess){
                System.out.println("You have guessed it!");
                break;
            }
            differenceNum = difference.getDifference(numToGuess, userNum);
            counter++;
            if(counter == 1){
                if(differenceNum == 0){
                    System.out.println("Impressive");
                    break;
                }else {
                    oldDifference = differenceNum;
                    System.out.println("Wrong. Tries left: " + (finalCounter - counter));
                }
            }
            if(counter > 1){
                tempDiff = oldDifference;
                currentDifference = differenceNum;
                oldDifference = currentDifference;
                hotCold = hotOrCold.isHotOrCold(tempDiff, currentDifference);
                System.out.println("Wrong, but " + hotCold + " Tries left: " + (finalCounter - counter));
            }

        }
    }
}
