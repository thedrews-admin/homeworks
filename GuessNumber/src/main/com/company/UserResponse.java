package main.com.company;

import java.util.Scanner;
import java.util.regex.Pattern;

public class UserResponse {

    public int getUserNum(String userResponse){
        try {
                boolean isNum = Pattern.matches("[0-9]+", userResponse);
                if(userResponse.length() > 3 || userResponse.length() < 1 && isNum){
                    System.out.println("Wrong input length");
                    return 0;
                }else {
                    int userNum = Integer.parseInt(userResponse);
                    if(userNum <= 100 && userNum > 0){
                        return userNum;
                    }else {
                        System.out.println("Wrong num");
                        return 0;
                    }
                }

        }catch (NumberFormatException e){
            System.out.println("Wrong symbols");
            return 0;
        }
    }
}
