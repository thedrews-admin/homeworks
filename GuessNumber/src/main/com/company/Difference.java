package main.com.company;

public class Difference {
    public int getDifference(int numToGuess, int userResponse){
        if(numToGuess > userResponse){
            return numToGuess - userResponse;
        }
        else if(userResponse > numToGuess){
            return userResponse - numToGuess;
        }else return 0;

    }
}
