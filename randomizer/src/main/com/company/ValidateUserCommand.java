package main.com.company;

import java.util.regex.Pattern;

public class ValidateUserCommand {
    boolean isAZ;
    public String validateUserCommand(String userResponse){
        isAZ = Pattern.matches("[a-zA-Z]+", userResponse);
        if(isAZ){
            if(userResponse.equalsIgnoreCase("generate")){
                return "generate";
            }else if(userResponse.equalsIgnoreCase("exit")){
                return "exit";
            }else if(userResponse.equalsIgnoreCase("help")){
                return "help";
            }
        }else {
            System.out.println("Incorrect command!");
        }
        return "error";
    }
}
