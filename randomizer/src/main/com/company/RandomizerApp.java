package main.com.company;
import java.util.Arrays;
import java.util.Scanner;
public class RandomizerApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ValidateUserNumResponse validateUserNumResponse = new ValidateUserNumResponse();
        ValidateUserCommand validateUserCommand = new ValidateUserCommand();
        GetHigherAndLowerNum getHigherAndLowerNum = new GetHigherAndLowerNum();
        Randomizer randomizer = new Randomizer();
        CheckPreviousGenerated checkPreviousGenerated = new CheckPreviousGenerated();

        String firstNumResponse, secondNumResponse, commandResponse, command = "";
        int startOfInterval, endOfInterval, interval, generatedNumber;
        boolean isRepeat;

        while (true) {  //get number 1
            System.out.println("Enter first number (1-500):");
            firstNumResponse = sc.next();
            startOfInterval = validateUserNumResponse.validateUserNumResponse(firstNumResponse);
            if(startOfInterval > 0 && startOfInterval <= 500){
                break;
            }else if(startOfInterval == 0){
                System.out.println("incorrect");
            }
        }

        while (true) {  //get number 2
            System.out.println("Enter second number (1-500):");
            secondNumResponse = sc.next();
            endOfInterval = validateUserNumResponse.validateUserNumResponse(secondNumResponse);
            if(endOfInterval != 0){
                break;
            }else{
                System.out.println("incorrect");
            }
        }

        getHigherAndLowerNum.getHigherAndLowerNum(startOfInterval, endOfInterval);
        startOfInterval= getHigherAndLowerNum.minNum;
        endOfInterval = getHigherAndLowerNum.maxNum;

        interval = endOfInterval - (startOfInterval-1);

        int[] filledArray = new int[interval];//filling array with numbers
        int startFilling = startOfInterval;
        for (int i = 0; i < filledArray.length; i++) {
            filledArray[i] = startFilling;
            System.out.print(filledArray[i] + " ");
            startFilling++;
        }

        int[] arrayToCompare = new int[interval];          //filling elements of array with 0
        for(int a : arrayToCompare){
            arrayToCompare[a] = 0;
        }

        while(!command.equalsIgnoreCase("exit")) {
            while (true) { //get command
                System.out.println("Enter command (generate, help, exit): ");
                commandResponse = sc.next();

                command = validateUserCommand.validateUserCommand(commandResponse);

                if(command.equals("generate") || command.equals("help") || command.equals("exit"))
                break;
            }

            if(command.equalsIgnoreCase("generate")){
                System.out.println("start: " + startOfInterval + " end " + endOfInterval);

                while (true) {
                    generatedNumber = randomizer.randomize(startOfInterval, interval);
                    isRepeat = checkPreviousGenerated.wasPreviousGenerated(filledArray, arrayToCompare, generatedNumber, startOfInterval);
                    if (!isRepeat) {
                        System.out.println(generatedNumber);
                        for (int a : arrayToCompare){
                            System.out.print(" " + a + " ");
                        }
                        break;
                    }
                    else {
                        if(Arrays.equals(filledArray, arrayToCompare)) {
                            System.out.println("Nothing left to generate!");
                            break;
                        }
                        System.out.println("GENERATED PREVIOUSLY");

                    }
                }
            }else if(command.equalsIgnoreCase("help")){
                System.out.println("Here could be your help list :) ");
            }
        }

    }
}
