package main.com.company;

import java.util.regex.Pattern;

public class ValidateUserNumResponse {
    public static boolean validatedNum;
    public static int parsedNum;
    public int validateUserNumResponse (String userResponse) {
        try {
            validatedNum = Pattern.matches("[0-9]+", userResponse);
            if (validatedNum) {
                parsedNum = Integer.parseInt(userResponse);
                if (parsedNum > 0 && parsedNum <= 500) {
                    return parsedNum;
                }
            }
            return 0;
        }catch (NullPointerException e){
            System.out.println("NullPointerException");
        }
        return 0;
    }
}
