package tests.com.company;

import main.com.company.ValidateUserCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


public class ValidateUserCommandTest {

    public static ValidateUserCommand cut = new ValidateUserCommand();
    static Arguments[] validateUserCommandTestArgs(){
        return new Arguments[]{
            Arguments.arguments("Exit", "exit" ),
            Arguments.arguments("genErate", "generate" ),
            Arguments.arguments("help", "help" ),
            Arguments.arguments("HELP", "help" ),
            Arguments.arguments("asasfasf", "error" ),
            Arguments.arguments("346346", "error" ),
            Arguments.arguments("sdg346", "error" ),
            Arguments.arguments("|]]]/.", "error" ),
            Arguments.arguments("      ", "error" ),
        };

    }

    @ParameterizedTest
    @MethodSource("validateUserCommandTestArgs")
    void validateUserCommandTest(String userResponse, String expected){
        String actual = cut.validateUserCommand(userResponse);
        Assertions.assertEquals(expected, actual);
    }

}
