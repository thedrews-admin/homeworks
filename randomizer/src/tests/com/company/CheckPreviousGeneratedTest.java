package tests.com.company;

import main.com.company.CheckPreviousGenerated;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class CheckPreviousGeneratedTest {
    private static final CheckPreviousGenerated cut = new CheckPreviousGenerated();
    static Arguments[] wasPreviousGeneratedTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{1, 2, 3, 4}, new int[]{0, 0, 0, 0}, 3, 1, false ),
                Arguments.arguments(new int[]{1, 2, 3, 4}, new int[]{0, 0, 3, 0}, 3, 1, true ),
                Arguments.arguments(new int[]{3, 4, 5, 6}, new int[]{0, 0, 5, 0}, 5, 3, true ),
                Arguments.arguments(new int[]{3, 4, 5, 6}, new int[]{3, 4, 5, 6}, 6, 3, true ),
                Arguments.arguments(new int[]{3, 4, 5, 6}, new int[]{3, 4, 5, 6}, 3, 3, true ),
                Arguments.arguments(new int[]{3, 4, 5, 6}, new int[]{0, 4, 5, 6}, 3, 3, false ),
        };
    }

    @ParameterizedTest
    @MethodSource("wasPreviousGeneratedTestArgs")
    void wasPreviousGeneratedTest(int[] arrA, int[] arrB, int numToCheck, int minNum, boolean expected){
        boolean actual = cut.wasPreviousGenerated(arrA, arrB, numToCheck, minNum);
        Assertions.assertEquals(expected, actual);
    }
}
