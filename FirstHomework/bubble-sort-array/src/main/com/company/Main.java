package main.com.company;

import java.util.Arrays;

public class Main{
    static void bubbleSort(int[] arrayUnsorted) {

        int counter = 0;

        for (int i = 0; i < arrayUnsorted.length - 1; i++)
            if (arrayUnsorted[i] > arrayUnsorted[i + 1]) {
                int temp = arrayUnsorted[i];
                arrayUnsorted[i] = arrayUnsorted[i + 1];
                arrayUnsorted[i + 1] = temp;
                counter++;
            }
        if (counter > 0) {
            bubbleSort(arrayUnsorted);
        }
    }

    public static void main(String[] args) {
        int nums[] = {10, 6, 4, 5, 2, 8, 3, 1, 9, 7};
        System.out.print("Array before sort: ");
        for(int a : nums){
            System.out.print(a + " ");
        }
        bubbleSort(nums);
        System.out.print("\nArray after sort: ");
        for (int b : nums){
            System.out.print(b + " ");
        }
    }
}
