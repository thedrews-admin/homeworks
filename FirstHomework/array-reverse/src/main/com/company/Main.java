package main.com.company;

public class Main
{
    public static void main(String[] args) {

        int nums[] = {1, 24, 3, 4, 46, 7, 8, 39, 10};
        int sizeArray = nums.length;

        System.out.println("Array before reverse: ");
        for(int i = 0; i < sizeArray; i++){
            System.out.print(" " + nums[i] + " ");
        }

        for(int i = 0; i < (sizeArray / 2); i++){
            int temporary = nums[i];
            nums[i] = nums[sizeArray - 1 - i];
            nums[sizeArray - 1 - i] = temporary;
        }

        System.out.println("\nArray after revesre: ");
        for(int i = 0; i < sizeArray; i++){
            System.out.print(" " + nums[i] + " ");
        }
    }

}