package main.com.company;

public class Main
{
    public static void main(String[] args) {
        int maxNum;

        int nums[] = {7, 5, 9, 1, 3};
        int sizeArray = nums.length;

        maxNum = nums[0];
        int maxNumIndex = nums[0];

        for(int i = 0; i < sizeArray; i++){
            int currentNum = nums[i];
            if(currentNum > maxNum){
                maxNum = currentNum;
                maxNumIndex = i;
            }
        }


        System.out.println("Maximal element of array is: " + maxNum);
        System.out.println("Index of maximal element of array is: " + maxNumIndex);
    }
}

