package test.com.company;

import main.com.company.Rating1_5;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class Rating1_5Test {
    Rating1_5 cut =  new Rating1_5();
    static Arguments[] getGradeByRatingTestArgs(){
        return new Arguments[]{
                Arguments.arguments(0, 'F'),
                Arguments.arguments(19, 'F'),
                Arguments.arguments(20, 'E'),
                Arguments.arguments(39, 'E'),
                Arguments.arguments(40, 'D'),
                Arguments.arguments(59, 'D'),
                Arguments.arguments(60, 'C'),
                Arguments.arguments(75, 'B'),
                Arguments.arguments(90, 'A'),
                Arguments.arguments(100, 'A'),
        };
    }
    @ParameterizedTest
    @MethodSource("getGradeByRatingTestArgs")
    void getGradeByRatingTest(int rate, char expGrade){
        int actual = cut.getGradeByRating(rate);
        Assertions.assertEquals(expGrade, actual);
    }
}
