package test.com.company;

import main.com.company.WhatPartOf1_2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class WhatPartOf1_2Test {
    WhatPartOf1_2 cut = new WhatPartOf1_2();
    static Arguments[] whatPartOfTestArgs(){
        return new Arguments[]{
                Arguments.arguments(-1, -1, "left bot"),
                Arguments.arguments(-1, 1, "left top"),
                Arguments.arguments(1, -1, "right bot"),
                Arguments.arguments(1, 1, "right top"),
                Arguments.arguments(0, 1, "rest part"),
        };
    }
    @ParameterizedTest
    @MethodSource("whatPartOfTestArgs")
    void whatPartOfTest(int x, int y, String expected){
        String actual = cut.whatPartOf(x, y);
        Assertions.assertEquals(expected, actual);
    }
}
