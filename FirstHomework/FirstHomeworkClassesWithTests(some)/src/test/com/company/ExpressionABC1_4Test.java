package test.com.company;

import main.com.company.ExpressionABC1_4;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ExpressionABC1_4Test {
    ExpressionABC1_4 cut = new ExpressionABC1_4();
    static Arguments[] getExpressionABCTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, 1, 1, 6),
                Arguments.arguments(1, 2, 3, 9),
                Arguments.arguments(-3, 2, 4, 6),
                Arguments.arguments(-3, -2, 4, 27),
                Arguments.arguments(2, 2, 2, 11),
        };
    }
    @ParameterizedTest
    @MethodSource("getExpressionABCTestArgs")
    void getExpressionABCTest(int a, int b, int c, int expected){
        int actual = cut.getExpressionABC(a, b, c);
        Assertions.assertEquals(expected, actual);
    }
}
