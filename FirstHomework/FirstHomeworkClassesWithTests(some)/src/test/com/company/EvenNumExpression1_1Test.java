package test.com.company;

import main.com.company.EvenNumExpression1_1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class EvenNumExpression1_1Test {
    EvenNumExpression1_1 cut = new EvenNumExpression1_1();
    static Arguments[] isEvenNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, 2, 5),
                Arguments.arguments(4, 2, 8),
        };
    }
    @ParameterizedTest
    @MethodSource("isEvenNumTestArgs")
    void isEvenNumTest(int a, int b, int expected){
        int actual = cut.isEvenNum(a, b);
        Assertions.assertEquals(expected, actual);
    }
}
