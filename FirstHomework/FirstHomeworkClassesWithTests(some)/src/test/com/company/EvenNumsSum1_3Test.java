package test.com.company;

import main.com.company.EvenNumsSum1_3;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class EvenNumsSum1_3Test {
    EvenNumsSum1_3 cut = new EvenNumsSum1_3();
    static Arguments[] getEvenSumTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2, 4, 6, 12),
                Arguments.arguments(2, 4, -1, 6),
                Arguments.arguments(4, -2, 3, 2),
                Arguments.arguments(0, -2, 3, -2),
                Arguments.arguments(3, 5, 7, 0),
        };
    }
        @ParameterizedTest
        @MethodSource("getEvenSumTestArgs")
        public void getEvenSumTest(int a, int b, int c, int expected){
            int actual = cut.getEvenSum(a, b, c);
            Assertions.assertEquals(expected, actual);
        }
}
