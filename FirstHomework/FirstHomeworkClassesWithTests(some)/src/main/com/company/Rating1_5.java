package main.com.company;

public class Rating1_5 {
    public char getGradeByRating(int rating) {
        if (rating >= 0 && rating < 20) {
            return 'F';
        } else if (rating >= 20 && rating < 40) {
            return 'E';
        } else if (rating >= 40 && rating < 60) {
            return 'D';
        } else if (rating >= 60 && rating < 75) {
            return 'C';
        } else if (rating >= 75 && rating < 90) {
            return 'B';
        } else if (rating >= 90 && rating <= 100) {
            return 'A';
        } else return '0';
    }
}
