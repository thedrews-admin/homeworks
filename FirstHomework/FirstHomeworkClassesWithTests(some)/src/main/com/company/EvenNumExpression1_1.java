package main.com.company;

public class EvenNumExpression1_1 {
    public int isEvenNum(int numA, int numB){
        if(numA % 2 == 0){
            return numA * numB;
        }else {
            return numA + numB;
        }
    }
}
