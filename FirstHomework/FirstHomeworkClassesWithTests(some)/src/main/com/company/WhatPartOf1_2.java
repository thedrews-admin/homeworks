package main.com.company;

public class WhatPartOf1_2 {
    public String whatPartOf(int x, int y){
        if(x < 0 && y < 0){
            return "left bot";
        }else if(x > 0 && y > 0){
            return "right top";
        }else if(x < 0 && y > 0){
            return "left top";
        }else if(x > 0 && y < 0){
            return "right bot";
        }else return "rest part";
    }
}
