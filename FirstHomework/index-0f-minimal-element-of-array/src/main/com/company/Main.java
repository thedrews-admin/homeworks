package main.com.company;

public class Main
{
    public static void main(String[] args) {
        int minNum;

        int nums[] = {7, 5, 9, 10, 3};
        int sizeArray = nums.length;

        minNum = nums[0];
        int minNumIndex = nums[0];

        for(int i = 0; i < sizeArray; i++){
            int currentNum = nums[i];
            if(currentNum < minNum){
                minNum = currentNum;
                minNumIndex = i;
            }
        }


        System.out.println("Minimal element of array is: " + minNum);
        System.out.println("Index of minimal element of array is: " + minNumIndex);
    }
}

