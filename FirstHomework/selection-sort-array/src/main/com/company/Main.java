package main.com.company;

import java.util.Arrays;

public class Main
{
    public static void selectionSort(int array[]){
        for(int i = 0; i < array.length - 1; i++)
        {
            int index = i;

            for(int j = i+1; j < array.length; j++)
            {
                if(array[j] < array[index])
                {
                    index = j;
                }
            }

            int smallerNum = array[index];
            array[index] = array[i];
            array[i] = smallerNum;
        }
    }

    public static void main(String[] args) {
        int numsArray[] = {14, 45, 32, 17, 1, 3};

        System.out.println("Array before selection sort: " + Arrays.toString(numsArray));

        selectionSort(numsArray);

        System.out.println("Array after selection sort: " + Arrays.toString(numsArray));

    }
}

