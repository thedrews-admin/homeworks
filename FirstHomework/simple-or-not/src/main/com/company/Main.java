package main.com.company;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userNum;

        System.out.println("Enter number: ");
        userNum = sc.nextInt();
        if(userNum == 2 || userNum == 3 || userNum == 5 || userNum == 7)
        {
            System.out.println("Your number is simple");
        }
        else
        {

            for(int i = 2; i<10; i++)
            {
                if((userNum % i) == 0)
                {
                    System.out.println("Your number is not simple");
                    break;
                }
                if(i == 9)
                {
                    System.out.println("Your number is simple");
                    break;
                }
            }
        }
    }
}

