package main.com.company;

public class Main
{
    public static void main(String[] args) {

        int nums[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int sizeArray = nums.length;
        int[] firstHalfNums = new int[sizeArray / 2];
        int[] secondHalfNums = new int[sizeArray / 2];

        System.out.println("Array before reverse: ");
        for(int i = 0; i < sizeArray; i++){
            System.out.print(" " + nums[i] + " ");
        }

        for(int i = 0; i < (sizeArray / 2); i++){
            firstHalfNums[i] = nums[i];
        }

        int ab = 0;
        for(int i = (sizeArray / 2); i < sizeArray; i++){
            secondHalfNums[ab] = nums[i];
            ab += 1;
        }

        for(int i = 0; i < (sizeArray / 2); i++){
            nums[i] = secondHalfNums[i];
        }
        int ba = 0;
        for(int i = (sizeArray / 2) ; i < sizeArray; i++){
            nums[i] = firstHalfNums[ba];
            ba += 1;
        }

        System.out.println("\nArray after reverse: ");
        for(int i = 0; i < sizeArray; i++){
            System.out.print(" " + nums[i] + " ");
        }

    }
}
