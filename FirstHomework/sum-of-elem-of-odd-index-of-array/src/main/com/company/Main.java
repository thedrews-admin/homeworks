package main.com.company;

public class Main
{
    public static void main(String[] args) {

        int nums[] = {7, 5, 9, 10, 3, 2, 1, 6, 4, 8};
        int sizeArray = nums.length;
        int sumNum = 0;


        for(int i = 1; i < sizeArray;){
            sumNum += nums[i];
            i += 2;
        }
        System.out.println("Sum of elements of odd indexes of array = " + sumNum);
    }

}

