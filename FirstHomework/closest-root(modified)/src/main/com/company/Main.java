package main.com.company;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userNum;
        int square =0;
        int guessRoot = 1;
        boolean stopCheck = true;

        System.out.println("Enter number: ");
        userNum = sc.nextInt();

        while(square <= userNum){

            if(square == userNum)
            {
                System.out.println("Square root = " + (guessRoot-1));
                stopCheck = false;
                break;
            }else{
                square = guessRoot * guessRoot;
                guessRoot++;
            }

        }

        int upperNum = (guessRoot-1) * (guessRoot-1);
        int lowerNum = (guessRoot-2) * (guessRoot-2);
        int upChange = upperNum - userNum;
        int downChange = userNum - lowerNum;
        if(stopCheck){

            if(upChange < downChange){
                System.out.println("Closest root = " + (guessRoot-1) + " from number: " + upperNum);
            }else
            {
                System.out.println("Closest root = " + (guessRoot-2) + " from number: " + lowerNum);
            }
        }
    }
}


