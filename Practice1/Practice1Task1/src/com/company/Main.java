package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
    int hundreds, tens, nums, counter1 = 0, counter2 = 0;


    System.out.println("Enter int number: ");
    int userResponse = sc.nextInt();
    String s = Integer.toString(userResponse);
    if(userResponse > 0 && userResponse < 1000){
        hundreds = userResponse / 100;
        tens = (userResponse % 100) / 10;
        nums = userResponse % 10;
        if(hundreds % 2 == 0){
            counter1++;
        }else counter2++;

        if(tens % 2 == 0){
            counter1++;
        }else counter2++;

        if(nums % 2 == 0){
            counter1++;
        }else counter2++;
    }

        System.out.println("You have entered: " + s);
        System.out.println("Even numbers = " + counter1);
        System.out.println("Odd numbers = " + counter2);



    }
}
