package com.company;

import java.util.Scanner;

public class NumToAscii {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter lower number: ");
        int userResponse1  = sc.nextInt();

        System.out.println("Enter higher number: ");
        int userResponse2 = sc.nextInt();

        for( int i = userResponse1; i <= userResponse2; i++){
            System.out.println(i + " = " +(char)i);
        }
    }
}
