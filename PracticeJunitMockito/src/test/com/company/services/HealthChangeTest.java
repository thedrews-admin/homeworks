package test.com.company.services;


import main.com.company.HealthChange;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class HealthChangeTest {

    private final HealthChange cut =  new HealthChange();

    static Arguments[] damageHealthTestArgs() {
        return new Arguments[]{
                Arguments.arguments(120, 0),
                Arguments.arguments(100, 0),
                Arguments.arguments(20, 80),
                Arguments.arguments(-100, 100),

        };
    }

    static Arguments[] increaseHealthTestArgs() {
        return new Arguments[]{
                Arguments.arguments(0, 120, 100),
                Arguments.arguments(0, 100, 100),
                Arguments.arguments(50, 32, 82),
                Arguments.arguments(50, -32, 50),
                Arguments.arguments(50, 0, 50)
        };
    }

    @ParameterizedTest
    @MethodSource("damageHealthTestArgs")
    public void damageHealthTest(int amount, int expected) {
        cut.damageHealth(amount);

        Assertions.assertEquals(expected, cut.health);

    }
    @ParameterizedTest
    @MethodSource("increaseHealthTestArgs")
    public void increaseHealthTest(int health, int amount, int expected) {
        cut.health = health;
        cut.increaseHealth(amount);

        Assertions.assertEquals(expected, cut.health);
    }

}
