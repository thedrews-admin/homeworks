package main.com.company;

import java.util.Scanner;

public class UserInstruction {
    String dmg = "damage", heal = "increase", showHp = "get";
    int getAmount;
    boolean check = false;
    String userResponse;
    Scanner sc = new Scanner(System.in);
    HealthChange action = new HealthChange();


    public void getUserInstruction(){
        do{
            System.out.println("Choose one action (damage, increase, get):");
            userResponse = sc.next();

            if(userResponse.equalsIgnoreCase(dmg)){

                System.out.println("Enter amount of damage or heal: ");
                getAmount = sc.nextInt();
                action.damageHealth(getAmount);

            }else if(userResponse.equalsIgnoreCase(heal)){

                System.out.println("Enter amount of damage or heal: ");
                getAmount = sc.nextInt();
                action.increaseHealth(getAmount);

            }else if(userResponse.equalsIgnoreCase(showHp)){
                action.getHealth();

            }else check = true;

        }while(check);

    }
}
