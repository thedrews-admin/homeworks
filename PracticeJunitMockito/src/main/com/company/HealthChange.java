package main.com.company;

public class HealthChange {
    public int health = 100;

    public void getHealth(){
        System.out.println("You have " + health + " health.");
    }

    public void damageHealth(int amount){
        if(amount >= 100){
            health = 0;
        }else if(amount < 0){
            health = health - 0;
        }else {
            health = health - amount;
            if (health < 0){
                health = 0;
            }
        }
    }
    public void increaseHealth(int amount){
        if(amount >= 100){
            health = 100;
        }else if(amount < 0){
            health = health + 0;
        }else {
            health = health + amount;
            if(health > 100){
                health = 100;
            }
        }
    }
}
