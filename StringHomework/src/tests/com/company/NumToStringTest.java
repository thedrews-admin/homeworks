package tests.com.company;

import main.com.company.NumToString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class NumToStringTest {
    NumToString cut = new NumToString();
    static Arguments[] numToStringTestArgsInt(){
        return new Arguments[]{
                Arguments.arguments(12, "12"),
                Arguments.arguments(-12, "-12"),
                Arguments.arguments(0, "0"),
        };
    }
    static Arguments[] numToStringTestArgsDouble(){
        return new Arguments[]{
                Arguments.arguments(12.32, "12.32"),
                Arguments.arguments(1, "1.0"),
                Arguments.arguments(0, "0.0"),
                Arguments.arguments(-12.32, "-12.32"),
        };
    }
    @ParameterizedTest
    @MethodSource("numToStringTestArgsInt")
    void numToStringTest(int num, String expected){
        String actual = cut.numToString(num);
        Assertions.assertEquals(expected, actual);
    }
    @ParameterizedTest
    @MethodSource("numToStringTestArgsDouble")
    void numToStringTest(double num, String expected){
        String actual = cut.numToString(num);
        Assertions.assertEquals(expected, actual);
    }
}
