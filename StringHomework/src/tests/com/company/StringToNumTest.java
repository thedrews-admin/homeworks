package tests.com.company;

import main.com.company.StringToNum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class StringToNumTest {
    public StringToNum cut = new StringToNum();

    static Arguments[] stringToDoubleTestArgs() {
        return new Arguments[]{
                Arguments.arguments("2.", 2.0),
                Arguments.arguments("2.0", 2.0),
                Arguments.arguments(".0", 0.0),
                Arguments.arguments("0.0", 0.0),
                Arguments.arguments("-1.0", -1.0),
        };
    }
    static Arguments[]stringToIntTestArgs(){
        return new Arguments[]{
                Arguments.arguments("12", 12),
                Arguments.arguments("0", 0),
                Arguments.arguments("-12", -12),
        };
    }

    @ParameterizedTest
    @MethodSource("stringToDoubleTestArgs")
    void stringToDoubleTest(String str, double expDouble) {
        double actual = cut.stringToDouble(str);
        Assertions.assertEquals(expDouble, actual);
    }

    @ParameterizedTest
    @MethodSource("stringToIntTestArgs")
    void stringToIntTest(String str, int expInt){
        int actual = cut.stringToInt(str);
        Assertions.assertEquals(expInt, actual);
    }
}
