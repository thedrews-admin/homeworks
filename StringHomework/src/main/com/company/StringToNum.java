package main.com.company;

public class StringToNum {
    public static double doubleNum;
    public static int intNum;

    public double stringToDouble(String str){
            return doubleNum = Double.parseDouble(str);
        }

    public int stringToInt(String str){
        return intNum = Integer.parseInt(str);
    }
}
