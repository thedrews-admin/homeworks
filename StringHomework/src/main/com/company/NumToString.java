package main.com.company;

public class NumToString {

    public static String numToString(int num){
        String str = Integer.toString(num);
        return str;
    }

    public static String numToString(double num){
        String str = Double.toString(num);
        return str;
    }
}
