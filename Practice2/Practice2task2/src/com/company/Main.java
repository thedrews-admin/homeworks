package com.company;

import java.util.Arrays;

public class Main {
    public static void getMinMaxNumOfArray ( int[][][] array1){
        int minNum = array1[0][0][0], maxNum = array1[0][0][0], temp;
        for (int i1 = 0; i1 < array1.length; i1++) {
            for (int i2 = 0; i2 < 2; i2++) {
                for (int i = 0; i < 7; i++) {
                    System.out.print(array1[i1][i2][i] + "  ");
                    temp = array1[i1][i2][i];
                    if (temp > maxNum) {
                        maxNum = temp;
                    } else if (temp < minNum) {
                        minNum = temp;
                    }
                    if (i == array1.length) {

                        System.out.print("   ");
                    }
                }
                if (i2 == 1) {
                    System.out.println();
                }
            }
        }
        System.out.println("Minimal number of array = " + minNum);
        System.out.println("Maximal number of array = " + maxNum);
    }

    public static void main(String[] args) {
        Main getMinMaxNumOfArray = new Main();
        int[][][] someArray = {
                {{12, 2, 3, 4, 5, 8, 7}, {1, 2, 3, 4, 5, 9, 7}},
                {{1, 2, 3, 4, 5, 6, 7}, {1, 2, 3, 4, 5, 1, 7}},
                {{1, 2, 3, 4, 5, 0, 7}, {1, 2, 3, 4, 5, 9, 7}},
                {{1, 2, 3, 4, 5, 6, 7}, {1, 2, 3, 4, 0, 6, 7}},
                {{1, 2, 3, 4, 5, 6, 7}, {1, 2, 3, 4, 5, 34, 7}},
                {{1, 2, 3, 4, 5, 6, 7}, {1, 2, 3, 4, 5, 6, 7}}
        };

        getMinMaxNumOfArray(someArray);
    }
}
